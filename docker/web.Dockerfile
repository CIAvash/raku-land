FROM caddy:2.4.6-alpine AS dev

COPY caddy /etc/caddy/

CMD ["caddy", "run", "--adapter", "caddyfile", "--config", "/etc/caddy/live.Caddyfile"]

FROM golang:1.18.3-alpine

ENV CGO_ENABLED=0 GOPATH=

COPY go.mod go.sum ./

RUN go mod download

COPY gmark ./

RUN go build -ldflags -s -trimpath -o gmark

FROM rakuland/raku:2022.06 AS dev

ENV RAKUDOLIB lib

WORKDIR /app

RUN mkdir /cache && chown nobody:nobody /cache \
 && apk add --no-cache alpine-sdk libpq openssl-dev rsync

COPY META6.json ./

RUN zef --deps-only --/test install .

COPY --from=0 /go/gmark /usr/local/bin/

FROM dev

COPY app.raku tag-mapping.toml ./
COPY bin                       ./bin
COPY css                       ./css
COPY js                        ./js
COPY lib                       ./lib
COPY static                    ./static
COPY svg                       ./svg
COPY views                     ./views

COPY --from=0 /go/gmark bin/

# Pre-compile the libraries, massively helps startup speed.
RUN raku -c app.raku

CMD ["raku", "app.raku"]

// Put time hover text in current locale.
document.querySelectorAll('time').forEach(
    e => e.title = new Date(e.dateTime).toLocaleString());

// Add autocomplete to the search box.
let controller;
document.forms[0].q.oninput = async e => {
    if (controller) controller.abort(); // Abort the old request (if exists).
    e.target.list.innerHTML = '';       // Clear current suggestions.

    if (e.target.value != '')
        e.target.list.append(...(await (await fetch(
            '/autocomplete?q=' + encodeURIComponent(e.target.value),
            { signal: (controller = new AbortController()).signal },
        )).json()).map(suggestion => {
            const option = document.createElement('option');
            option.value = suggestion;
            return option;
        }));
};

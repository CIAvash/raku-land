unit module DateTime::Relative;

use MONKEY-TYPING;

augment class DateTime {
    method relative of Str {
        my $duration = self - DateTime.now;
        my $relative = do given $duration.abs.round {
            my constant min  = 60;
            my constant hour = 60 * min;
            my constant day  = 24 * hour;

            when * <  2 * min  { 'a min' }
            when * <  1 * hour { round( $_ / min ) ~ ' mins' }
            when * <  2 * hour { 'an hour' }
            when * <  1 * day  { round( $_ / hour ) ~ ' hours' }
            when * <  2 * day  { 'a day' }
            when * < 28 * day  { round( $_ / day ) ~ ' days' }
            default {
                my constant months =
                    <Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec>;

                return join ' ', $.day, months[$.month - 1], $.year;
            }
        }

        return $duration < 0 ?? "$relative ago" !! "in $relative";
    }
}

unit module Local::Routes::Stats;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;

get -> 'stats' {
    my %stats = db.query(q:to/SQL/).hashes.classify: *.<fact>;
        (SELECT 'Ecosystem' fact, eco::text "value", COUNT(*)
           FROM distinct_dists
       GROUP BY value
       ORDER BY value)

      UNION ALL

        (SELECT 'Raku' fact, perl::text "value", COUNT(*)
           FROM distinct_dists
       GROUP BY value
       ORDER BY value)

      UNION ALL

        (SELECT 'Year' fact, extract(year from date)::text "value", COUNT(*)
           FROM distinct_dists
       GROUP BY value
       ORDER BY value)
    SQL

    for %stats.values {
        my $total = $_».<count>.sum;

        .<percent> = sprintf '%.1f', .<count> / $total * 100 for $_[];
    }

    template 'stats.crotmp', { :%stats };
}

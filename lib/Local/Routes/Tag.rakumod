unit module Local::Routes::Tag;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;
use Local::Pager;
use TOML::Thumb;

my %tag-mapping = 'tag-mapping.toml'.IO.&from-toml.invert;

get sub ("tags", $tag, PosInt :$page = 1) {
    # Redirect known mappings, case-insensitively.
    # TODO perm redirect.
    .&redirect.return with %tag-mapping{$tag.lc};

    # TODO Match insensitively, redirect if they differ.
    my @dists = db.query( q:to/SQL/, $tag, ( $page - 1 ) * 10 ).hashes;
        SELECT date, description, eco, name, slug, stars, tags,
               COUNT(*) OVER()
          FROM distinct_dists
         WHERE $1 = ANY(tags)
      ORDER BY stars DESC NULLS LAST, name
         LIMIT 10 OFFSET $2
    SQL

    return not-found unless @dists;

    my $pager = Local::Pager.new :$page :total(@dists[0]<count> // 0);
    my @tags  = db.query(q:to/SQL/).hashes;
        WITH tags AS (
            SELECT author_id, name, unnest(tags) tag FROM distinct_dists
        ) SELECT tag, COUNT(*) count
            FROM tags
        GROUP BY tag
          HAVING COUNT(DISTINCT author_id) > 1 AND COUNT(DISTINCT name) > 1
        ORDER BY tag
    SQL

    template 'tags.crotmp', { :@dists, :$pager, :$tag, :@tags };
}

unit module Local::DB;

use DB::Pg;

my \db = DB::Pg.new;

sub db is export { db }

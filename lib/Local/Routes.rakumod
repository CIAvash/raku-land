unit module Local::Routes;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Crypt::Rand;
use Local::DB;
use MIME::Base64;

template-location 'views/', :compile-all;

&render-template.wrap: -> $template, %topic, :%parts {
    state %css = dir("css").map: { .extension("").basename => .slurp };
    state %js  = dir("js" ).map: { .extension("").basename => .slurp };
    state %svg = dir("svg").map: { .extension("").basename => .slurp };

    # The generated value SHOULD be at least 128 bits long (before encoding),
    # and SHOULD be generated via a cryptographically secure random number
    # generator - https://w3c.github.io/webappsec-csp/#security-nonces
    my $nonce = MIME::Base64.encode: crypt-rand 16;

    # Remove any previous CSP header in case we're now printing a 500 to a
    # previously attempted template render.
    $*CRO-ROUTER-RESPONSE.remove-header: 'Content-Security-Policy';

    header 'Content-Security-Policy', join ';',
        "base-uri 'none'",
        "connect-src 'self'",
        "default-src 'none'",
        "frame-ancestors 'none'",
        "img-src data: https:",
        "script-src 'nonce-$nonce'",
        "style-src 'nonce-$nonce'";

    callwith $template, %( |%topic, :%svg ), :parts(%( |%parts,
        # CSS, JS, & nonce.
        :css-js(\( :$nonce,
            :css(%css<common> ~ (%css{ ~$template.IO.extension: '' } // '')),
             :js( %js<common> ~ ( %js{ ~$template.IO.extension: '' } // '')),
        )),

        # Ingestion date & ID.
        :ingest(\( |db.query("SELECT * FROM ingest").hash )),
    ));
}

our $application is export = route {
    after {
        # Custom error pages.
        content 'text/html', render-template 'error.crotmp', %(
            :title(.get-response-phrase.subst: 'Server responded with '),
        ) if .status >= 400 && !.has-body;
    }

    # Require all the routes.
    for dir 'lib/Local/Routes', :test(*.ends-with: '.rakumod') {
        require ::("Local::Routes::{.extension('').basename}");
    }

    get { static 'static', @_ }
}

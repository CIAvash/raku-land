module gitlab.com/raku-land/raku-land

go 1.18

require (
	github.com/alecthomas/chroma v0.9.2
	github.com/microcosm-cc/bluemonday v1.0.18
	github.com/tdewolff/minify/v2 v2.11.2
	github.com/yuin/goldmark v1.4.12
	github.com/yuin/goldmark-emoji v1.0.1
	github.com/yuin/goldmark-highlighting v0.0.0-20210516132338-9216f9c5aa01
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/tdewolff/parse/v2 v2.5.29 // indirect
	golang.org/x/net v0.0.0-20220526153639-5463443f8c37 // indirect
)
